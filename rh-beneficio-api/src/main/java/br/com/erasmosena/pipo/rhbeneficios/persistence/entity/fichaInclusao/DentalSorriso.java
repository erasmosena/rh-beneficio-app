package br.com.erasmosena.pipo.rhbeneficios.persistence.entity.fichaInclusao;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class DentalSorriso extends FichaInclusaoBeneficio {

    @NotEmpty
    private String nome;

    @DecimalMin(value= "0.1")
    private double peso;

    @DecimalMin(value= "0.1")
    private double altura;




}
