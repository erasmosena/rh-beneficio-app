package br.com.erasmosena.pipo.rhbeneficios.enums;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum EnumBeneficio {
    NORTE_EUROPA("NorteEuropa"),
    PAMPULHA_INTERMEDICA("PampulhaIntermedica"),
    DENTAL_SORRISO("DentalSorriso"),
    MENTE_SA_CORPO_SAO("MenteSaCorpoSao");

    private String value;

    EnumBeneficio(String value){
        this.value = value;
    }


}
