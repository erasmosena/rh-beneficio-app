package br.com.erasmosena.pipo.rhbeneficios.resources;


import br.com.erasmosena.pipo.rhbeneficios.dto.FichaInclusaoFuncionarioDTO;
import br.com.erasmosena.pipo.rhbeneficios.dto.ResponseDTO;
import br.com.erasmosena.pipo.rhbeneficios.mapper.RHMapper;
import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.Cliente;
import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.fichaInclusao.DentalSorriso;
import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.fichaInclusao.FichaInclusaoBeneficio;
import br.com.erasmosena.pipo.rhbeneficios.services.BeneficioService;
import br.com.erasmosena.pipo.rhbeneficios.services.ClienteService;
import br.com.erasmosena.pipo.rhbeneficios.utils.CustomValidator;
import br.com.erasmosena.pipo.rhbeneficios.utils.FactoryFichaInclusao;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/beneficio")
public class FuncionarioResource {

    private final ClienteService clienteService;
    private final BeneficioService funcionarioService;

    private  RHMapper mapper;
    private final FactoryFichaInclusao factoryFichaInclusao;

    @PostMapping("/incluir")
    public ResponseEntity<ResponseDTO> incluirFuncionario( @RequestBody FichaInclusaoFuncionarioDTO dto){

        CustomValidator validator = new CustomValidator();
        List<String> erros = new ArrayList<>();

        Cliente cliente = clienteService.findById(dto.getIdCliente()).orElse(null);

        List<FichaInclusaoBeneficio> fichas = cliente.getBeneficios().stream()
                .map(plano ->factoryFichaInclusao.createFichaInclusao(plano.getEnumBeneficio(), dto))
                .collect(Collectors.toList());

        fichas.forEach( ficha -> erros.addAll( validator.getErros(ficha)));

        if( erros.size() > 0 )
            return ResponseEntity.badRequest().body( new ResponseDTO(new ArrayList<>(), erros.stream().distinct().collect(Collectors.toList())));

        List<FichaInclusaoBeneficio> beneficios = funcionarioService.addBeneficios(fichas);
        return ResponseEntity.ok(new ResponseDTO<List<FichaInclusaoBeneficio>>( beneficios , new ArrayList<>() ));
    }

    @GetMapping()
    public ResponseEntity<ResponseDTO> obterFuncionario(){

        return ResponseEntity.ok(new ResponseDTO<List<FichaInclusaoBeneficio>>(  funcionarioService.getBeneficios() , new ArrayList<>() ));
    }

}
