package br.com.erasmosena.pipo.rhbeneficios.persistence.repository;

import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.fichaInclusao.FichaInclusaoBeneficio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BeneficioRepository extends JpaRepository<FichaInclusaoBeneficio, Integer> {

    @Query(value= " SELECT count(fi.id_ficha_inclusao_beneficio) > 0  FROM ficha_inclusao_beneficio fi where fi.cpf = :cpf and fi.tipo_beneficio = :tipoBeneficio",
            nativeQuery = true)
    public boolean existsByCpfAndTipo(String cpf, String tipoBeneficio);

}
