package br.com.erasmosena.pipo.rhbeneficios.dto;

import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.Endereco;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NonNull;

import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FichaInclusaoFuncionarioDTO {

    Integer idCliente;

    String cpf;
    String nome;
    String email;
    double peso;
    double altura;
    double horasMeditadasUltimosSeteDias;
    String dataAdmissao;

    //Endereco endereco;
    String endereco;
    String tipo ;
}
