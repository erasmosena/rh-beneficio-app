package br.com.erasmosena.pipo.rhbeneficios.persistence.entity;

import br.com.erasmosena.pipo.rhbeneficios.enums.EnumBeneficio;
import lombok.*;

import javax.persistence.*;


@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Beneficio {

    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idBeneficio;

    private String nome;
    private String camposObrigatorios;

    @Enumerated(EnumType.STRING)
    private EnumBeneficio enumBeneficio;

}
