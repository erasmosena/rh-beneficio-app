package br.com.erasmosena.pipo.rhbeneficios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class RhBeneficiosApplication {

	public static void main(String[] args) {
		SpringApplication.run(RhBeneficiosApplication.class, args);
	}
}
