package br.com.erasmosena.pipo.rhbeneficios.persistence.entity.fichaInclusao;

import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.Endereco;
import lombok.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class PampulhaIntermedica extends FichaInclusaoBeneficio {

    @NotEmpty
    private String nome ;

    @NotNull
    private LocalDate dataAdmissao;

    @NotEmpty
    private String endereco;
//    @OneToOne(cascade= CascadeType.PERSIST)
//    @JoinColumn(name = "endereco_id_endereco")
//    @NotNull
//    private Endereco endereco;
}
