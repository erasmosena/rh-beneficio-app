package br.com.erasmosena.pipo.rhbeneficios.persistence.entity.fichaInclusao;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.*;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@DiscriminatorColumn(name="tipo_beneficio",length = 50)
public class FichaInclusaoBeneficio {

    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer idFichaInclusaoBeneficio;

    @NotEmpty(message = "O campo não pode ser vazio")
    @Length(min = 11, max = 11, message = "O campo deve ter 11 caracteres")
    @Pattern(regexp = "[\\s]*[0-9]*[1-9]+",message="O campo CPF aceita apenas números")
    protected String cpf;

    @Transient
    protected String tipo;





}
