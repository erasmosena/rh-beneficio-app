package br.com.erasmosena.pipo.rhbeneficios.mapper;

import br.com.erasmosena.pipo.rhbeneficios.dto.ClienteDTO;
import br.com.erasmosena.pipo.rhbeneficios.dto.FichaInclusaoFuncionarioDTO;
import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.Cliente;
import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.fichaInclusao.DentalSorriso;
import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.fichaInclusao.MenteSaCorpoSao;
import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.fichaInclusao.NorteEuropa;
import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.fichaInclusao.PampulhaIntermedica;
import org.mapstruct.Mapper;


@Mapper(componentModel="spring")
public interface RHMapper {

    DentalSorriso toFichaInclusaoFuncionarioDentalSorriso(FichaInclusaoFuncionarioDTO dto );
    NorteEuropa toFichaInclusaoFuncionarioNorteEuropa(FichaInclusaoFuncionarioDTO dto );
    PampulhaIntermedica toFichaInclusaoFuncionarioPampulhaIntermedica(FichaInclusaoFuncionarioDTO dto );
    MenteSaCorpoSao toFichaInclusaoFuncionarioMenteSaCorpoSao(FichaInclusaoFuncionarioDTO dto );

    ClienteDTO toDTO(Cliente entity);

}
