package br.com.erasmosena.pipo.rhbeneficios.services;

import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.fichaInclusao.FichaInclusaoBeneficio;
import br.com.erasmosena.pipo.rhbeneficios.persistence.repository.BeneficioRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BeneficioService {

    private final BeneficioRepository beneficioRepository;

    public List<FichaInclusaoBeneficio> addBeneficios(List<FichaInclusaoBeneficio> fichas){
        List<FichaInclusaoBeneficio> lista = new ArrayList<>();
        fichas.stream().forEach(it -> {

            if( !beneficioRepository.existsByCpfAndTipo(it.getCpf(),it.getTipo())) {
                lista.add(beneficioRepository.save(it));
            }
        });
        return lista;
    }

    public List<FichaInclusaoBeneficio> getBeneficios(){
        return beneficioRepository.findAll();

    }

}
