package br.com.erasmosena.pipo.rhbeneficios.services;

import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.Cliente;
import br.com.erasmosena.pipo.rhbeneficios.persistence.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private  ClienteRepository clienteRepository;

    public List<Cliente> getAll (){
         List<Cliente> lista = this.clienteRepository.findAll();
         return lista;
    }

    public Optional<Cliente> findById(int  id) {
        Optional<Cliente> cliente =  this.clienteRepository.findById(id);
        return cliente ;
    }
}
