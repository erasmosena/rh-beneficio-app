package br.com.erasmosena.pipo.rhbeneficios.dto;

import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.Beneficio;
import lombok.Data;

import java.util.List;

@Data
public class ClienteDTO {
    private Integer idCliente;
    private String nome ;
    List<Beneficio> beneficios;
}
