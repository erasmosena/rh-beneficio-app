package br.com.erasmosena.pipo.rhbeneficios.persistence.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Endereco {
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idEndereco;

    @NotEmpty
    private String logradouro;

    @NotEmpty
    private String numero;

    @NotEmpty
    private String cidade;

    @NotEmpty
    private String uf;

}
