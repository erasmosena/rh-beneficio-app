package br.com.erasmosena.pipo.rhbeneficios.persistence.entity.fichaInclusao;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity

public class MenteSaCorpoSao extends FichaInclusaoBeneficio {

    private double horasMeditadasUltimosSeteDias;
}
