package br.com.erasmosena.pipo.rhbeneficios.utils;

import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.fichaInclusao.FichaInclusaoBeneficio;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CustomValidator {

    ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
    Validator validator = validatorFactory.usingContext().getValidator();

    public List<String> getErros(FichaInclusaoBeneficio ficha ){

        Set<ConstraintViolation<FichaInclusaoBeneficio>> constrains = validator.validate(ficha);
        return constrains.stream()
                .map(it -> "[" + it.getPropertyPath() + "][" + it.getMessage() + "]")
                .collect(Collectors.toList());

    }
}
