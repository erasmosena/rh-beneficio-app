package br.com.erasmosena.pipo.rhbeneficios.utils;

import br.com.erasmosena.pipo.rhbeneficios.dto.FichaInclusaoFuncionarioDTO;
import br.com.erasmosena.pipo.rhbeneficios.enums.EnumBeneficio;
import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.fichaInclusao.FichaInclusaoBeneficio;
import br.com.erasmosena.pipo.rhbeneficios.mapper.RHMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FactoryFichaInclusao {

    private final RHMapper mapper ;

    public  FichaInclusaoBeneficio createFichaInclusao(EnumBeneficio beneficio, FichaInclusaoFuncionarioDTO dto){
        dto.setTipo(beneficio.getValue());
        switch (beneficio){

            case NORTE_EUROPA :{
                return mapper.toFichaInclusaoFuncionarioNorteEuropa(dto);
            }

            case PAMPULHA_INTERMEDICA :{
                return mapper.toFichaInclusaoFuncionarioPampulhaIntermedica(dto);
            }

            case DENTAL_SORRISO:{
                return mapper.toFichaInclusaoFuncionarioDentalSorriso(dto);
            }

            case MENTE_SA_CORPO_SAO:{
                return mapper.toFichaInclusaoFuncionarioMenteSaCorpoSao(dto);
            }

            default:
                return null;
        }
    }

}
