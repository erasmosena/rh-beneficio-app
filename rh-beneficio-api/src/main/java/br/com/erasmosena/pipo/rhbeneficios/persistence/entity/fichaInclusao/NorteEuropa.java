package br.com.erasmosena.pipo.rhbeneficios.persistence.entity.fichaInclusao;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class NorteEuropa extends FichaInclusaoBeneficio {

    @NotEmpty
    private String nome;

    @NotNull
    private LocalDate dataAdmissao;

    @NotEmpty
    private String email;
}
