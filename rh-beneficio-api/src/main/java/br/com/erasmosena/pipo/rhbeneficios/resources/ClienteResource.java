package br.com.erasmosena.pipo.rhbeneficios.resources;

import br.com.erasmosena.pipo.rhbeneficios.dto.ClienteDTO;
import br.com.erasmosena.pipo.rhbeneficios.dto.ResponseDTO;
import br.com.erasmosena.pipo.rhbeneficios.mapper.RHMapper;
import br.com.erasmosena.pipo.rhbeneficios.persistence.entity.Cliente;
import br.com.erasmosena.pipo.rhbeneficios.services.ClienteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/cliente")
public class ClienteResource {

    private final ClienteService clienteService;
    private final RHMapper mapper;

    @GetMapping()
    public ResponseEntity<ResponseDTO> obterFuncionario(){
        List<ClienteDTO> lista = clienteService.getAll().stream()
                .map( it -> mapper.toDTO(it)).collect(Collectors.toList());
        return ResponseEntity.ok(new ResponseDTO<List<ClienteDTO>>( lista , null ));
    }


}
