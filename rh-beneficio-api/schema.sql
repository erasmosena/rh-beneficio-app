 create table beneficio (id_beneficio  serial not null, campos_obrigatorios varchar(255), enum_beneficio varchar(255), nome varchar(255), primary key (id_beneficio))
 create table cliente (id_cliente  serial not null, nome varchar(255), primary key (id_cliente))
 create table cliente_beneficio (cliente_id int4 not null, beneficio_id int4 not null)
 create table endereco (id_endereco  serial not null, cidade varchar(255), logradouro varchar(255), numero varchar(255), uf varchar(255), primary key (id_endereco))
 create table ficha_inclusao_beneficio (tipo_beneficio varchar(50) not null, id_ficha_inclusao_beneficio  serial not null, cpf varchar(11), altura float8, nome varchar(255), peso float8, horas_meditadas_ultimos_sete_dias float8, data_admissao date, email varchar(255), endereco varchar(255), primary key (id_ficha_inclusao_beneficio))
 alter table ficha_inclusao_beneficio add constraint UK_8vc60n0r59j206fmcla5md56 unique (cpf)
 alter table cliente_beneficio add constraint FK547xwejsmxr0amrifgkmqk8ma foreign key (beneficio_id) references beneficio
alter table cliente_beneficio add constraint FKjrhh8hvxlghjeomhn22anjrli foreign key (cliente_id) references cliente


INSERT INTO public.beneficio(id_beneficio, nome, enum_beneficio ,campos_obrigatorios)
VALUES (1, 'NorteEuropa', 'NORTE_EUROPA','cpf,nome,dataAdmissao,email');
INSERT INTO public.beneficio(id_beneficio, nome, enum_beneficio ,campos_obrigatorios)
VALUES (2, 'Pampulha Intermédica', 'PAMPULHA_INTERMEDICA','cpf,nome,dataAdmissao,endereco');
INSERT INTO public.beneficio(id_beneficio, nome, enum_beneficio ,campos_obrigatorios)
VALUES (3, 'Dental Sorriso', 'DENTAL_SORRISO','cpf,nome,peso,altura');
INSERT INTO public.beneficio(id_beneficio, nome, enum_beneficio ,campos_obrigatorios)
VALUES (4, 'ente Sã, Corpo São', 'MENTE_SA_CORPO_SAO','cpf,horasMeditadasUltimosSeteDias');


INSERT INTO public.cliente(id_cliente, nome)VALUES (1, 'Acme Co');

INSERT INTO public.cliente_beneficio(cliente_id, beneficio_id)VALUES (1, 1);
INSERT INTO public.cliente_beneficio(cliente_id, beneficio_id)VALUES (1, 3);

INSERT INTO public.cliente(id_cliente, nome)VALUES (2, 'Tio Patinhas Bank');

INSERT INTO public.cliente_beneficio(cliente_id, beneficio_id)VALUES (2, 2);
INSERT INTO public.cliente_beneficio(cliente_id, beneficio_id)VALUES (2, 3);
INSERT INTO public.cliente_beneficio(cliente_id, beneficio_id)VALUES (2, 4);