cd rh-beneficio-api 
mvn package -DskipTests
docker build . -t rh-api:latest

cd ..

cd rh-beneficio-app 
ng build 
docker build . -t rh-app:latest 

cd ..

docker-compose up 

