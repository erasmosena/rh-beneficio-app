export const CAMPO_NOME = 'nome';
export const CAMPO_EMAIL = 'email';
export const CAMPO_PESO = 'peso';
export const CAMPO_ALTURA = 'altura';
export const CAMPO_HORAS_MEDITADAS = 'horasMeditadasUltimosSeteDias';
export const CAMPO_DATA_ADMISSAO = 'dataAdmissao';
export const CAMPO_ENDERECO = 'endereco';