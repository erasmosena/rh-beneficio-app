

  export const CPF_FUNCIONARIO_MESSAGES={
    required: 'Informe o CPF.',
    cpf: 'CPF em formato inválido.'
  }

  export const EMAIL_FUNCIONARIO_MESSAGES={
    required: 'Informe o E-mail.',
    email: 'E-mail em formato inválido.'
  }
  
  export const CLIENTE_FUNCIONARIO_MESSAGES ={
    required: 'Informe o cliente.'
  }

  export const NOME_FUNCIONARIO_MESSAGES ={
    required: 'Informe o nome.',
    rangeLength: 'O nome deve possuir entre 2 e 100 caracteres.',    
  }

  export const ENDERECO_FUNCIONARIO_MESSAGES ={
    required: 'Informe o endereço.',
  }

  export const PESO_FUNCIONARIO_MESSAGES = {
    required: 'Informe o peso.'
  }

  export const ALTURA_FUNCIONARIO_MESSAGES = {
    required: 'Informe a altura.'
  }

  export const HORAS_MEDITACAO_FUNCIONARIO_MESSAGES = {
    required: 'Informe o número de horas meditadas nos ultimos 7 dias.'
  }

  export const DATA_ADMISSAO_FUNCIONARIO_MESSAGES = {
    required: 'Informe a data de admissão.'
  }
  




// endereço separado 
  export const LOGRADOURO_FUNCIONARIO_MESSAGES ={
    required: 'Informe o logradouro',
  }
  
  export const NUMERO_FUNCIONARIO_MESSAGES ={
    required: 'Informe o número',
  }

  export const CIDADE_FUNCIONARIO_MESSAGES ={
    required: 'Informe a cidade',
  }

  export const ESTADO_FUNCIONARIO_MESSAGES ={
    required: 'Informe o estado',
  }


