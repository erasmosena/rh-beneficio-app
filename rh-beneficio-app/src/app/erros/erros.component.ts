import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-erros',
  templateUrl: './erros.component.html',
  styleUrls: ['./erros.component.css']
})
export class ErrosComponent implements OnInit {

  @Input() 
  errors:any[] = []
  constructor() { }

  ngOnInit(): void {
  }

}
