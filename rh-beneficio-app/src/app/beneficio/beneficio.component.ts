import { Component, ElementRef, Inject, LOCALE_ID, OnInit, ViewChildren } from '@angular/core';
import { AbstractControl, FormBuilder, FormControlName, FormGroup, ValidationErrors, Validator, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomValidators } from '@narik/custom-validators';
import { utilsBr } from 'js-brasil'
import { NgBrazilValidators } from 'ng-brazil';
import { ToastrService } from 'ngx-toastr';
import { fromEvent, merge, Observable } from 'rxjs';
import { StringUtils } from '../utils/string-utils';
import { DisplayMessage, GenericValidator, ValidationMessages } from '../validations/generic-form-validator';
import { ALTURA_FUNCIONARIO_MESSAGES, CIDADE_FUNCIONARIO_MESSAGES, CLIENTE_FUNCIONARIO_MESSAGES, CPF_FUNCIONARIO_MESSAGES, DATA_ADMISSAO_FUNCIONARIO_MESSAGES, EMAIL_FUNCIONARIO_MESSAGES, ENDERECO_FUNCIONARIO_MESSAGES, ESTADO_FUNCIONARIO_MESSAGES, HORAS_MEDITACAO_FUNCIONARIO_MESSAGES, LOGRADOURO_FUNCIONARIO_MESSAGES, NOME_FUNCIONARIO_MESSAGES, NUMERO_FUNCIONARIO_MESSAGES, PESO_FUNCIONARIO_MESSAGES } from '../validations/messages-form-validation';
import { TextMaskModule } from 'angular2-text-mask';
import { InclusaoBeneficio } from './inclusao-beneficio.model';
import { BeneficioService } from './services/beneficio.service';
import { Cliente } from './model/cliente.model';
import { DatePipe, formatDate } from '@angular/common';
import * as moment from 'moment';
import { CAMPO_ALTURA, CAMPO_DATA_ADMISSAO, CAMPO_EMAIL, CAMPO_ENDERECO, CAMPO_HORAS_MEDITADAS, CAMPO_NOME, CAMPO_PESO } from '../validations/campos';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';


@Component({
  selector: 'app-beneficio',
  templateUrl: './beneficio.component.html',
  styleUrls: ['./beneficio.component.css']
})
export class BeneficioComponent implements OnInit {

  @ViewChildren(FormControlName, { read: ElementRef })
  formInputElements: ElementRef[]


  MASKS = utilsBr.MASKS
  funcionario: InclusaoBeneficio
  clientes: Cliente[]
  mudancasNaoSalvas: boolean
  form: FormGroup
  errors: any[] = []

  validationMessages: ValidationMessages
  genericValidator: GenericValidator
  displayMessage: DisplayMessage

  processando = false 

  constructor(private fb: FormBuilder,
    private service: BeneficioService,
    private route:ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,) {


    this.validationMessages = {
      idCliente: CLIENTE_FUNCIONARIO_MESSAGES,
      email: EMAIL_FUNCIONARIO_MESSAGES,
      nome: NOME_FUNCIONARIO_MESSAGES,
      cpf: CPF_FUNCIONARIO_MESSAGES,
      endereco: ENDERECO_FUNCIONARIO_MESSAGES,
      peso: PESO_FUNCIONARIO_MESSAGES,
      altura: ALTURA_FUNCIONARIO_MESSAGES,
      horasMeditadasUltimosSeteDias:HORAS_MEDITACAO_FUNCIONARIO_MESSAGES,
      dataAdmissao:DATA_ADMISSAO_FUNCIONARIO_MESSAGES,
    }

    this.genericValidator = new GenericValidator(this.validationMessages);

    this.clientes = this.route.snapshot.data['clientes']
  }

  get f() {
    return this.form
  }


  getMask(){
    return {mask:this.MASKS.cpf.textMask}
  }


  ngOnInit(): void {
    this.form = this.fb.group({
      idCliente:['',[
        Validators.required]],
      cpf: ['', [
        Validators.required,
        NgBrazilValidators.cpf,
      ]],
      email: [''],
      nome:[''],
      peso: [''],
      altura: [''],
      horasMeditadasUltimosSeteDias:[''],
      dataAdmissao:[''],
      endereco:['']
    })
  }

  ngAfterViewInit(): void {

    this.f.get('idCliente')?.valueChanges.subscribe(
      (idCliente )=>{
        this.applicarValidacaoControle(idCliente)
        //this.configurarElementosValidacao()
        this.validarFormulario()
      })

    this.configurarElementosValidacao()
  }

  configurarElementosValidacao(){
    let controlBlurs: Observable<any>[] = this.formInputElements
      .map((formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur'));

    merge(...controlBlurs).subscribe(() => {
     this.validarFormulario()
    })


  }


  validarFormulario(){
    this.displayMessage = this.genericValidator.processarMessagens(this.f)
    this.mudancasNaoSalvas = true
  }

  applicarValidacaoControle(idCliente:number){
    let campos = [...new Set(this.clientes.filter(it => it.idCliente == idCliente)[0]?.beneficios.flatMap(it => it.camposObrigatorios.split(',')))]
    this.limparCampos()
    
    campos.forEach( 
      (campo:string) => {
        this.f.get(campo)?.setValidators(this.getValidator(campo))
        this.f.get(campo)?.updateValueAndValidity()
      }
    )
  }
 
  private limparCampos(){
    let campos = [CAMPO_NOME,CAMPO_EMAIL,CAMPO_PESO,CAMPO_ALTURA,CAMPO_HORAS_MEDITADAS,CAMPO_DATA_ADMISSAO,CAMPO_ENDERECO]
    campos.forEach( it => {
      this.f.get(it)?.clearValidators()
      this.f.get(it)?.setErrors([])
    })
    
  }
  
  private getValidator(campo:string ):ValidatorFn[]{
    switch (campo) {
      case CAMPO_EMAIL : return  [ Validators.email, Validators.required]
      case CAMPO_PESO: return  [ Validators.min(0), Validators.required]
      case CAMPO_ALTURA: return  [ Validators.min(0), Validators.required]
      case CAMPO_HORAS_MEDITADAS: return  [ Validators.min(0), Validators.required]
      case CAMPO_ENDERECO: return  [ Validators.minLength(3), Validators.required]
      default:
        return [Validators.required]
    }    
  }
  
  adicionarFuncionario() {
    
    if (this.f.dirty && this.f.valid) {
      this.processando = true 
      this.funcionario = Object.assign({}, this.funcionario, this.f.value)
      this.funcionario.altura = parseFloat(this.funcionario.altura.toString())
      this.funcionario.peso = parseFloat(this.funcionario.peso.toString())
      this.funcionario.horasMeditadasUltimosSeteDias = parseFloat(this.funcionario.horasMeditadasUltimosSeteDias.toString())

      this.funcionario.cpf = StringUtils.somenteNumeros(this.funcionario.cpf)
      const datepipe: DatePipe = new DatePipe('en-US')
      this.funcionario.dataAdmissao = moment(this.funcionario.dataAdmissao, "DD/MM/YYYY").format("YYYY-MM-DD")
      
      this.service.novoFuncionario(this.funcionario).subscribe(
        sucesso => { this.processarSucesso(sucesso) },
        falha => { this.processarFalha(falha) }
      )

    }
  }

  processarSucesso(response: any) {
    this.f.reset()
    this.errors = []
    this.toastr.success('Registro realizado com sucesso', ':)')
    this.mudancasNaoSalvas = false
    this.processando = false 
  }

  processarFalha(fail: any) {
    this.errors = fail.error.erros
    this.toastr.error('Ocorreu um erro', 'Opa! :(')
    this.processando = false
  }

}
