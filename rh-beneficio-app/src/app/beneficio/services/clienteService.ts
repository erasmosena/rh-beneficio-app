import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, map, Observable } from "rxjs";
import { BaseService } from "src/app/BaseService";
import { Cliente } from "../model/cliente.model";


@Injectable({
    providedIn: 'root'
  })
export class ClienteService extends BaseService{
    override resource = "cliente"

    constructor(private http: HttpClient) {
        super()
      }

      obterTodos(): Observable<Cliente[]> {
        
        let response = this.http
          .get(`${this.urlServiceV1}/${this.resource}`)
          .pipe(
            map(this.extractData),
            catchError(this.serviceError)
          )
        return response
      }
    
}