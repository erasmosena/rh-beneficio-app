import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, map, Observable } from "rxjs";
import { BaseService } from "src/app/BaseService";
import { InclusaoBeneficio } from "../inclusao-beneficio.model";


@Injectable({
    providedIn: 'root'
  })
export class BeneficioService extends BaseService{
    override resource = "beneficio"

    constructor(private http: HttpClient) {
        super()
      }

    novoFuncionario(funcionario: InclusaoBeneficio): Observable<InclusaoBeneficio> {
        
        let response = this.http
          .post(`${this.urlServiceV1}/${this.resource}/incluir`, funcionario, this.obterHeaderJson())
          .pipe(
            map(this.extractData),
            catchError(this.serviceError)
          )
        return response
      }
    
}