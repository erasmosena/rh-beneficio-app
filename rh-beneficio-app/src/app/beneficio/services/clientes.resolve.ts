import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve } from "@angular/router";

import { Cliente } from "../model/cliente.model";

import { BeneficioService } from "./beneficio.service";
import { ClienteService } from "./clienteService";

@Injectable()
export class ClientesResolve implements Resolve<Cliente[]>{
    constructor(private service: ClienteService){}

    resolve(route: ActivatedRouteSnapshot) {
        return this.service.obterTodos()
    }
}

