import { Endereco } from "./model/endereco.model";

export interface InclusaoBeneficio{
    idCliente: number
    cpf:string 
    nome:string
    email:string
    peso:number
    altura:number
    horasMeditadasUltimosSeteDias:number
    dataAdmissao:string | null
    endereco:string 

}
