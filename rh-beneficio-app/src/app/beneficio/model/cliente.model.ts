import { Beneficio } from "./beneficio.model";

export interface Cliente{
    idCliente:number
    nome:String 
    beneficios: Beneficio[]
}