import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import ptBr from '@angular/common/locales/pt';
registerLocaleData(ptBr)
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { ToastrModule } from 'ngx-toastr';
import { BeneficioComponent } from './beneficio/beneficio.component';
import { HomeComponent } from './home/home.component';
import { ErrosComponent } from './erros/erros.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgBrazil } from 'ng-brazil';
import { TextMaskModule } from 'angular2-text-mask';
import { CommonModule, registerLocaleData } from '@angular/common';
import { NarikCustomValidatorsModule } from '@narik/custom-validators';
import { HttpClientModule } from '@angular/common/http';
import { ClienteService } from './beneficio/services/clienteService';
import { BeneficioService } from './beneficio/services/beneficio.service';
import { ClientesResolve } from './beneficio/services/clientes.resolve';
import { FormDebugComponent } from './utils/forms-debug/form-debug.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    FooterComponent,
    BeneficioComponent,
    ErrosComponent,
    FormDebugComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,

    NgbModule,
    NarikCustomValidatorsModule,
    TextMaskModule,
    NgBrazil,    
    ToastrModule.forRoot(),

    AppRoutingModule,     
    
  ],
  providers: [
    ClientesResolve,
    BeneficioService,
    ClienteService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
