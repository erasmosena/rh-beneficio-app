import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BeneficioComponent } from './beneficio/beneficio.component';
import { ClientesResolve } from './beneficio/services/clientes.resolve';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo:'/home', pathMatch:'full'},
  { path: 'home', component: HomeComponent},
  { 
    path: 'funcionario', 
    component: BeneficioComponent, 
    resolve:{
      clientes: ClientesResolve
    }
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
