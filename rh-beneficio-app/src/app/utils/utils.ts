export class Utils{
    static mapearFiltro( filtro:{[key:string]:string}):string{
        let params = ''
        if( filtro !== null ){
            for( let key in filtro){
                if(params.length> 0 )params = `&${params}`
                else params = `?${params}`
                params = `${params}${key}=${filtro[key]}`
            }
        }
        return params;
    }
}